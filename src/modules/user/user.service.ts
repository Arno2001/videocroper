import { ConflictException, Injectable } from '@nestjs/common';
import type { UserRegisterDto } from '../auth/dto/user-register.dto';
import { UserDto } from './dto/user.dto';
import { User } from './user.schema';
import { Model, Types } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name)
    private readonly userModel: Model<User>,
  ) {}

  async createUser(userRegisterDto: UserRegisterDto): Promise<User> {
    const userExists = await this.findByEmail(userRegisterDto.email);
    if (userExists) {
      throw new ConflictException();
    }

    const user = await this.userModel.create(userRegisterDto);

    return user;
  }

  async findByEmail(email: string): Promise<User | null> {
    return this.userModel.findOne().where('email').equals(email).exec();
  }

  async findById(id: Types.ObjectId): Promise<User | null> {
    const userId = new Types.ObjectId(id);
    return this.userModel.findOne().where('_id').equals(userId).exec();
  }

  async getAll(): Promise<UserDto[]> {
    const users = await this.userModel.find().exec();

    return users.map((item) => new UserDto(item));
  }

  async delete(id: Types.ObjectId): Promise<void> {
    const userId = new Types.ObjectId(id);
    await this.userModel.deleteOne().where('_id').equals(userId);
  }
}
