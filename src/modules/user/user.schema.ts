import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { UseDto } from '../../decorators';
import { UserDto } from './dto/user.dto';
import { AbstractSchema } from '../../common/abstract.schema';
import { TimestampTypeEnum } from '../../constants/timestamp-type.enum';

export type UserDocument = HydratedDocument<User>;

@Schema({
  collection: 'user',
  timestamps: TimestampTypeEnum,
})
@UseDto(UserDto)
export class User extends AbstractSchema {
  @Prop({ type: 'string' })
  name: string;

  @Prop({ type: 'string' })
  surname: string;

  @Prop({ type: 'string' })
  email: string;

  @Prop({ type: 'string' })
  password: string;
}

export const userSchema = SchemaFactory.createForClass(User);
