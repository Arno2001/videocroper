import { ApiProperty } from '@nestjs/swagger';

import { AbstractDto } from '../../../common/dto/abstract.dto';
import type { User } from '../user.schema';

export class UserDto extends AbstractDto {
  @ApiProperty()
  name: string;

  @ApiProperty()
  surname: string;

  @ApiProperty()
  email: string;

  constructor(user: User) {
    super(user);
    this.name = user.name;
    this.surname = user.surname;
    this.email = user.email;
  }
}
