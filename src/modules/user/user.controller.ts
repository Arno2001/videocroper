import {
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
} from '@nestjs/common';
import { ApiOkResponse, ApiResponse, ApiTags } from '@nestjs/swagger';

import { Auth } from '../../decorators';
import { UserDto } from './dto/user.dto';
import { UserService } from './user.service';
import { Types } from 'mongoose';

@Controller('users')
@ApiTags('users')
export class UserController {
  constructor(private userService: UserService) {}

  @Get()
  @Auth()
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    description: 'Get users list',
    type: UserDto,
  })
  async getAll(): Promise<UserDto[]> {
    return this.userService.getAll();
  }

  @Delete(':id')
  @Auth()
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Delete user',
    type: UserDto,
  })
  delete(@Param('id') userId: Types.ObjectId): Promise<void> {
    return this.userService.delete(userId);
  }
}
