import {
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  UploadedFile,
} from '@nestjs/common';
import { ApiNoContentResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';

import { ApiFile, Auth } from '../../decorators';
import { CropService } from './crop.service';
import { IFile } from '../../interfaces';
import { CropRedisDto } from './dto/crop-redis.dto';
import { CropMongoDto } from './dto/crop-mongo.dto';

@Controller('crop')
@ApiTags('crop')
export class CropController {
  constructor(private cropService: CropService) {}

  @Post()
  @Auth()
  @HttpCode(HttpStatus.CREATED)
  @ApiOkResponse({
    description: 'Video Successfully is croped',
  })
  @ApiFile({ name: 'video' })
  async create(
    @UploadedFile() file: IFile,
  ): Promise<Record<string, CropRedisDto | CropMongoDto>> {
    return this.cropService.create(file);
  }

  @Get()
  @Auth()
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    description: 'Get videos list',
  })
  async getAll(): Promise<CropRedisDto[]> {
    return this.cropService.getAll();
  }

  @Delete(':id')
  @Auth()
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiNoContentResponse({
    status: HttpStatus.OK,
    description: 'Delete videos',
  })
  delete(@Param('id') id: string): Promise<void> {
    return this.cropService.delete(id);
  }
}
