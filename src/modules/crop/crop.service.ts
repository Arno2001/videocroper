import { Injectable } from '@nestjs/common';
import { Model, Types } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Crop, cropDocument } from './crop.schema';
import { IFile } from '../../interfaces';

import * as ffmpeg from 'fluent-ffmpeg';
import * as ffmpegInstaller from '@ffmpeg-installer/ffmpeg';
import * as ffprobePath from 'ffprobe-static';
import * as path from 'path';
import { v4 as uuid } from 'uuid';
import { Readable } from 'stream';
import { InjectRedis } from '@nestjs-modules/ioredis';
import { Redis } from 'ioredis';
import { VideoPartTypeEnum } from '../../constants/video-part-type.enum';
import { unlinkSync } from 'fs';
import { CropMongoDto } from './dto/crop-mongo.dto';
import { CropRedisDto } from './dto/crop-redis.dto';
import { VideoDataNotFoundException } from './exceptions/video-data-not-found.exception';
import { VideoIsShorterException } from './exceptions/video-is-shorter.exception';

ffmpeg.setFfmpegPath(ffmpegInstaller.path);
ffmpeg.setFfprobePath(ffprobePath.path);

@Injectable()
export class CropService {
  constructor(
    @InjectModel(Crop.name)
    private readonly cropModel: Model<cropDocument>,
    @InjectRedis()
    private redis: Redis,
  ) {}

  async getVideoDuration(buffer: Buffer): Promise<number> {
    return new Promise((resolve, reject) => {
      ffmpeg(Readable.from(buffer)).ffprobe((error, metadata) => {
        if (error) {
          reject(error);
          return;
        }
        const durationVideo = metadata.format.duration;

        if (durationVideo < 600) {
          reject(new VideoIsShorterException());
        }

        resolve(durationVideo);
      });
    });
  }

  async setStartVideo(buffer: Buffer): Promise<string> {
    const duration = await this.getVideoDuration(buffer);
    const last5Minutes = 5 * 60;
    const startTime = Math.max(duration - last5Minutes, 0);

    const hours = Math.floor(startTime / 3600)
      .toString()
      .padStart(2, '0');
    const minutes = Math.floor((startTime % 3600) / 60)
      .toString()
      .padStart(2, '0');
    const seconds = Math.floor(startTime % 60)
      .toString()
      .padStart(2, '0');

    return `${hours}:${minutes}:${seconds}`;
  }

  async crop(
    file: IFile,
    options: {
      videoPart: VideoPartTypeEnum;
      startTime: string;
    },
  ): Promise<string> {
    const extension: string = path.parse(file.originalname).ext;
    const destination = 'uploads/';
    const videoName =
      options.videoPart === VideoPartTypeEnum.FIRST
        ? `firtsFiveMinutes_${uuid()}${extension}`
        : `lastFiveMinutes_${uuid()}${extension}`;

    ffmpeg(Readable.from(file.buffer))
      .setStartTime(options.startTime)
      .setDuration('00:05:00')
      .output(destination + videoName)
      .on('error', (error) => new Error(error))
      .run();

    return videoName;
  }
  async create(
    file: IFile,
  ): Promise<Record<string, CropRedisDto | CropMongoDto>> {
    const startTime = await this.setStartVideo(file.buffer);
    const first = await this.crop(file, {
      videoPart: VideoPartTypeEnum.FIRST,
      startTime: '00:00:00',
    });
    const last = await this.crop(file, {
      videoPart: VideoPartTypeEnum.LAST,
      startTime,
    });

    const extension: string = file.originalname.split('.')[1];
    const firstPartData = await this.cropModel.create({
      name: first,
      timestamp: new Date(),
      origin: file.originalname,
      type: extension,
    });

    const lastPartData: CropRedisDto = {
      id: firstPartData._id,
      name: last,
      timestamp: new Date(),
      origin: file.originalname,
      type: extension,
    };
    await this.redis.set(String(lastPartData.id), JSON.stringify(lastPartData));

    const resData = {
      firstPartData: new CropMongoDto(firstPartData),
      lastPartData: new CropRedisDto(lastPartData),
    };
    return resData;
  }

  async getAll(): Promise<CropRedisDto[]> {
    const videos = new Set();
    const redisSize = await this.redis.dbsize();

    if (redisSize <= 10) {
      const redisKeys = await this.redis.keys('*');
      const redisValues = await this.redis.mget(redisKeys);

      return [...redisValues].map((value: string) => JSON.parse(value));
    }

    while (videos.size < 10) {
      const randomData = await this.redis.get(await this.redis.randomkey());
      videos.add(randomData);
    }

    return [...videos.values()].map((value: string) => JSON.parse(value));
  }

  async findById(id: string): Promise<CropMongoDto> {
    const videoId = new Types.ObjectId(id);
    const videoData = await this.cropModel
      .findOne()
      .where('_id')
      .equals(videoId);

    if (!videoData) {
      throw new VideoDataNotFoundException();
    }
    return videoData;
  }

  async delete(id: string): Promise<void> {
    // const key = await this.redis.randomkey();
    const firstVideoData = await this.findById(id);
    const lastVideoData = JSON.parse(await this.redis.get(id));
    const videoId = new Types.ObjectId(id);

    await this.cropModel.deleteOne().where('_id').equals(videoId);
    await this.redis.del(id);

    unlinkSync(`uploads/${firstVideoData.name}`);
    unlinkSync(`uploads/${lastVideoData.name}`);
  }
}
