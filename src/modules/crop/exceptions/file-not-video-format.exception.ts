import { BadRequestException } from '@nestjs/common';

export class FileNotVideoFormatException extends BadRequestException {
  constructor() {
    super('error.fileNotVideoFormat');
  }
}
