import { BadRequestException } from '@nestjs/common';

export class VideoIsShorterException extends BadRequestException {
  constructor() {
    super('error.videoIsShorter');
  }
}
