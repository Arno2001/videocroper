import { NotFoundException } from '@nestjs/common';

export class VideoDataNotFoundException extends NotFoundException {
  constructor() {
    super('error.videoDataNotFound');
  }
}
