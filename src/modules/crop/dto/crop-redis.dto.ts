import { Types } from 'mongoose';

export class CropRedisDto {
  id: Types.ObjectId;
  name: string;
  timestamp: Date;
  origin: string;
  type: string;

  constructor(crop: CropRedisDto) {
    this.id = crop.id;
    this.name = crop.name;
    this.timestamp = crop.timestamp;
    this.origin = crop.origin;
    this.type = crop.type;
  }
}
