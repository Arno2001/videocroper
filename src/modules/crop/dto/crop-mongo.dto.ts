import { ApiProperty } from '@nestjs/swagger';

import { AbstractDto } from '../../../common/dto/abstract.dto';
import type { Crop } from '../crop.schema';

export class CropMongoDto extends AbstractDto {
  @ApiProperty()
  name: string;

  @ApiProperty()
  timestamp: Date;

  @ApiProperty()
  origin: string;

  @ApiProperty()
  type: string;

  constructor(crop: Crop) {
    super(crop);
    this.name = crop.name;
    this.timestamp = crop.timestamp;
    this.origin = crop.origin;
    this.type = crop.type;
  }
}
