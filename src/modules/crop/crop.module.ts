import { Module } from '@nestjs/common';

import { CropController } from './crop.controller';
import { CropService } from './crop.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Crop, cropSchema } from './crop.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Crop.name, schema: cropSchema }]),
  ],
  controllers: [CropController],
  exports: [CropService],
  providers: [CropService],
})
export class CropModule {}
