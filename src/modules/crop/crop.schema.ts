import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { UseDto } from '../../decorators';
import { AbstractSchema } from '../../common/abstract.schema';
import { CropMongoDto } from './dto/crop-mongo.dto';
import { TimestampTypeEnum } from '../../constants/timestamp-type.enum';

export type cropDocument = HydratedDocument<Crop>;

@Schema({
  collection: 'crop',
  timestamps: TimestampTypeEnum,
})
@UseDto(CropMongoDto)
export class Crop extends AbstractSchema {
  @Prop({ type: 'string' })
  name: string;

  @Prop({ type: Date })
  timestamp: Date;

  @Prop({ type: 'string' })
  origin: string;

  @Prop({ type: 'string' })
  type: string;
}
export const cropSchema = SchemaFactory.createForClass(Crop);
