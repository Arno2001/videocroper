import { Types } from 'mongoose';

export interface IAccessTokenPayload {
  userId: Types.ObjectId;
}
