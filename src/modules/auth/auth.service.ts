import { Injectable } from '@nestjs/common';

import { UtilsProvider } from '../../providers';
import { JwtService } from '../../shared/services/jwt.service';
import { UserDto } from '../user/dto/user.dto';
import { UserService } from '../user/user.service';
import { LoginPayloadDto } from './dto/login-payload.dto';
import type { UserLoginDto } from './dto/user-login.dto';
import type { UserRegisterDto } from './dto/user-register.dto';
import { InvalidPasswordException, UserNotFoundException } from './exceptions';
import { Types } from 'mongoose';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    private userService: UserService,
  ) {}

  async register(userRegisterDto: UserRegisterDto): Promise<LoginPayloadDto> {
    userRegisterDto.password = UtilsProvider.generateHash(
      userRegisterDto.password,
    );
    const userSchema = await this.userService.createUser(userRegisterDto);
    const accessToken = this.jwtService.createAccessToken(userSchema);

    return new LoginPayloadDto({
      accessToken,
      user: new UserDto(userSchema),
    });
  }

  async login(userLoginDto: UserLoginDto): Promise<LoginPayloadDto> {
    const userSchema = await this.userService.findByEmail(userLoginDto.email);
    if (!userSchema) {
      throw new UserNotFoundException();
    }
    const isPasswordValid = await UtilsProvider.validateHash(
      userLoginDto.password,
      userSchema.password,
    );

    if (!isPasswordValid) {
      throw new InvalidPasswordException();
    }

    const accessToken = this.jwtService.createAccessToken(userSchema);

    return new LoginPayloadDto({ accessToken, user: new UserDto(userSchema) });
  }

  async authMe(userId: Types.ObjectId): Promise<UserDto> {
    const user = await this.userService.findById(userId);

    if (!user) {
      throw new UserNotFoundException();
    }

    return new UserDto(user);
  }
}
