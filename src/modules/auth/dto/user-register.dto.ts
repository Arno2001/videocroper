import { EmailField, PasswordField, StringField } from '../../../decorators';

export class UserRegisterDto {
  @StringField()
  name: string;

  @StringField()
  surname: string;

  @EmailField()
  email: string;

  @PasswordField()
  password: string;
}
