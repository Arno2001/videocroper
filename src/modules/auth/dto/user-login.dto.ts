import { EmailField, PasswordField } from '../../../decorators';

export class UserLoginDto {
  @EmailField()
  email: string;

  @PasswordField()
  password: string;
}
