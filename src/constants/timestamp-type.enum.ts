export enum TimestampTypeEnum {
  createdAt = 'createdAt',
  updatedAt = 'updatedAt',
}
