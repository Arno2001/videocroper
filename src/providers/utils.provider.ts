import * as bcrypt from 'bcryptjs';

export class UtilsProvider {
  /**
   * generate hash from password or string
   * @param {string} password
   * @returns {string}
   */
  static generateHash(password: string): string {
    return bcrypt.hashSync(password, 10);
  }

  /**
   * validate text with hash
   * @param {string} password
   * @param {string} hash
   * @returns {Promise<boolean>}
   */
  static validateHash(password: string, hash?: string): Promise<boolean> {
    if (!hash) {
      return Promise.resolve(false);
    }

    return bcrypt.compare(password, hash);
  }
}
