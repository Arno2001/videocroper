import { FileNotVideoFormatException } from '../modules/crop/exceptions/file-not-video-format.exception';

export class ValidateProvider {
  static fileOptions = () => ({
    fileFilter: (req, file: Express.Multer.File, cb) => {
      if (!file.originalname.match(/\.(mp4|avi|mkv)$/i)) {
        cb(new FileNotVideoFormatException(), file.filename);
      }
      cb(null, file);
    },
  });

  static videoFileMiddlware = this.fileOptions();
}
