export interface IFile {
  encoding: string;
  buffer: Buffer;
  fieldname: string;
  mimetype: string;
  originalname: string;
  destination: string;
  filename: string;
  path: string;
  size: number;
}
