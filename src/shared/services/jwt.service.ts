import { Injectable } from '@nestjs/common';
import jwt, { sign } from 'jsonwebtoken';

import { UserTokenFotFoundException } from '../../modules/auth/exceptions';
import type { IAccessTokenPayload } from '../../modules/auth/interfaces/IAccessTokenPayload';
import { ApiConfigService } from './api-config.service';
import { UserDto } from '../../modules/user/dto/user.dto';

@Injectable()
export class JwtService {
  readonly jwtPrivateKey: string;

  readonly jwtPublicKey: string;

  constructor(public apiConfigService: ApiConfigService) {
    this.jwtPrivateKey = apiConfigService.authConfig.privateKey;
    this.jwtPublicKey = apiConfigService.authConfig.publicKey;
  }

  createAccessToken(user: UserDto): string {
    const payload: IAccessTokenPayload = {
      userId: user._id,
    };
    const options: jwt.SignOptions = {
      algorithm: 'RS256',
    };

    return sign(payload, this.jwtPrivateKey, options);
  }

  decodeToken<T>(token: string): T {
    try {
      return <T>jwt.decode(token);
    } catch {
      throw new UserTokenFotFoundException();
    }
  }
}
