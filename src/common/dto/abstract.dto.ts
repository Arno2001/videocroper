import { ApiProperty } from '@nestjs/swagger';
import { Types } from 'mongoose';
import { AbstractSchema } from '../abstract.schema';

export class AbstractDto {
  @ApiProperty()
  _id: Types.ObjectId;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;

  protected constructor(schema: AbstractSchema) {
    this._id = schema._id;
    this.createdAt = schema.createdAt;
    this.updatedAt = schema.updatedAt;
  }
}
