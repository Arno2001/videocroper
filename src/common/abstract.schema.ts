import { Prop } from '@nestjs/mongoose';
import { Types } from 'mongoose';

export abstract class AbstractSchema {
  @Prop({
    type: Types.ObjectId,
    default: () => new Types.ObjectId(),
    required: true,
  })
  declare _id: Types.ObjectId;

  @Prop({ type: Date })
  declare createdAt: Date;

  @Prop({ type: Date })
  declare updatedAt: Date;
}
